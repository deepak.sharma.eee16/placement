import sys
from osgeo import gdal  #geospatial data package
import argparse     #for using argument parser
#import requests
import os        #for path resolution purposes
import errno
import boto3    # for using s3 resources
import botocore    # for using s3 resources
import ntplib
import requests
import json
import time
import datetime
import rasterio       #geospatial related library
from rasterio.profiles import Profile
from rio_cogeo import profiles
from rasterio.io import MemoryFile
from rio_cogeo import cogeo
from rio_cogeo.cogeo import cog_translate, cog_validate
from rio_cogeo.profiles import COGProfiles
from rasterio.rio import options
from daymark import daymark
import rasterio as rio
import numpy as np
import json
import gc
import hist_func
import subprocess
from rasterio import MemoryFile    # for chunking/memory utilization purposes


redisEndPoint = daymark.daymark.getEnvVar("REDIS_ENDPOINT")
global redisInstance
redisInstance = daymark.daymark.init(redisEndPoint)
global id
id = daymark.daymark.getEnvVar("JOBID")
global s3Resource

s3 = boto3.client('s3')

global t1, t2, t3, t4, t5, t6, t7, t8

#uploading the files to AWS bucket  
def upload(output_bucket, key, path):  
    
    try:
        if not os.path.exists(path):
            print('file not exist')
        else:
           
            s3Path = "s3://" + output_bucket + "/" + key        
            subprocess.call(["aws", "s3" ,"cp", path , s3Path])
    except Exception as e:
        print(e)
        os.remove(path)
        daymark.daymark.logError(e, id, redisInstance)
        print('error while uploading')

def download(input_bucket, key, ortho_name):

    try:
        file_path = '/lighthouse/' + ortho_name
        dir_name = os.path.dirname(file_path)
        s3 = boto3.resource('s3')
        try:
            if not os.path.exists(dir_name):
                os.makedirs(dir_name) 
            s3.Bucket(input_bucket).download_file(key,file_path)

        except OSError:
            print ('Error: Creating directory. ' + key) 
            raise Exception('Error: Creating directory. ' + key)

        except Exception as e:
            print(e)
            print('error while downloading')

    except Exception as e:
            print('error')

            
if __name__ == '__main__':

    try:

        parser = argparse.ArgumentParser()
        parser.add_argument('--input_bucket')
        parser.add_argument('--output_bucket')      
        parser.add_argument('--key')
        s3 = boto3.resource('s3')
        input_bucket = parser.parse_args().input_bucket
        output_bucket = parser.parse_args().output_bucket
        key = parser.parse_args().key
        output_key = key + '/'       
        ortho_name = key.split('/')[-1]

        t1= time.time()
        
        if ortho_name.endswith('.tif'):
            print('TIFF file found')
            download(input_bucket, key, ortho_name)
            print('File downloading complete')
        
        t2= time.time()
        print('time taken by download')
        print(t2 - t1)
        path_input = '/lighthouse/' + ortho_name
    
        file_path_output = os.path.join(os.path.dirname(path_input), 'index.tif')
        meta_path_output = os.path.join(os.path.dirname(path_input), 'metadata.json')
    
        try:
                     
            cogeo.cog_translate(path_input ,file_path_output , dict(driver = "GTiff", interleave = "pixel", tiled = True,BIGTIFF= "YES", BIGTIFF_OVERVIEW = "YES" ,NUM_THREADS= "ALL_CPUS" , blockxsize = 512,  blockysize =  512,  compress = "LZW" , ) )
            
            t4= time.time()
            print('time taken by conversion')
            print(t4 - t2)
            if cogeo.cog_validate(file_path_output ):
                print('validate after conversion')
                t5= time.time()
                print('time taken by validator of cogeo tiff')
                print(t5 - t4)
            else:
                print('not validated after conversion')

        except Exception as e:
            daymark.daymark.logError(e, id, redisInstance)
            print(e)
            print('error while validation')
        
        t6= time.time()
 
        file_key = output_key + "index.tif"
        meta_key = output_key + "metadata.json"

        
        upload(output_bucket, file_key, file_path_output)
        upload(output_bucket, meta_key, meta_path_output)
        
        print('file upload complete')
        t8= time.time()
        print('time taken by upload')
        print(t8 - t7)
        
        os.remove(file_path_output)
        os.remove(meta_path_output)
        
        daymark.daymark.logSuccessful("Input file successfully processed", id, redisInstance)
        
    except Exception as e:
        print('Function Failed')
        daymark.daymark.logError(e, id, redisInstance)








